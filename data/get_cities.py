import urllib
import sys
import json
from urllib.request import urlopen
import urllib.parse
import string
import json
import re
import pandas as pd
import requests

articleUrl = "https://es.wikipedia.org/wiki/Armando_Rub%C3%A9n_Becerra_Flores"

def extract_language_and_article(url):
    # Define a regular expression pattern to match Wikipedia URLs
    pattern = r'https://([a-zA-Z]+)\.wikipedia\.org/wiki/([^/]+)'

    # Use re.match to find matches in the URL
    match = re.match(pattern, url)

    if match:
        # Extract the language and article name from the matched groups
        language = match.group(1)
        article_name = match.group(2)

        return language, article_name
    else:
        print("Invalid Wikipedia URL")

def extract_first_term(wikitext):
    # Define a regular expression pattern to match terms between [[ and ]]
    pattern1 = r'\(\[\[([^|\]]+)\]\]\,'
    pattern2 = r'\[\[([^|\]]+)'

    # Use re.search to find the first match in the wikitext
    match1 = re.search(pattern1, wikitext)
    match2 = re.search(pattern2, wikitext)

    if match1:
        # Extract the term from the matched group
        first_term = match1.group(1)
        print(first_term)
        return first_term
    elif match2:
    	# Extract the term from the matched group
        first_term = match2.group(1)
        return first_term
    else:
        print("No match found")


def get_wikitext(articleUrl):
	try:
		print(articleUrl)
		language, article_name = extract_language_and_article(articleUrl)

		url = "https://"+language+".wikipedia.org/w/api.php?action=parse&page=" + article_name + "&prop=wikitext&formatversion=2&format=json"

		json_url = urlopen(url)
		data = json.loads(json_url.read())
		wikitext = data["parse"]["wikitext"]
		#return first 1499 characters
		result = wikitext[:1499]
	except:
		result = ""

	return result

# Read the CSV file into a pandas DataFrame
input_csv_path = 'tiles_202311131600.csv'
df = pd.read_csv(input_csv_path,header=0,sep='|')

# Apply the extract_first_term function to the 'wikitext' column and create a new 'first_term' column
print(df.columns);
df['primer_parrafo'] = df['artículo'].apply(get_wikitext)
df['lugar_sugerido'] = df['primer_parrafo'].apply(extract_first_term)

# Save the DataFrame with the new column to a new CSV file
output_csv_path = 'your_output_csv.csv'
df.to_csv(output_csv_path, index=False)

print("CSV file with new column saved:", output_csv_path)

def get_qids():
	df['lugar_sugerido_qid'] = df['lugar_sugerido'].apply(search_first_match)
