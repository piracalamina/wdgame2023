// server.js
require('dotenv').config({path:__dirname + '/.env'});
const express = require('express')
const mysql = require('mysql')
const connection = mysql.createConnection({
  host: `${process.env.DB_HOST}`,
  user: `${process.env.DB_USER}`,
  password: `${process.env.DB_PASSWORD}`,
  database: `${process.env.DB_NAME}`
})
connection.connect();

const STATUS_NOT_PLAYED = 0;
const STATUS_SOLVED = 1;
const STATUS_INVALID = 2;

const app = express()
const port = process.env.PORT || 3000;

app.get('/', handleRequest)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

 async function createTiles(rows) {
  let tiles = await Promise.all(
    rows.map(async(val) => {
      let entries = await createOptions(val.qid,val.pais)
      entries.push({ "type": "white", "decision": "skip", "label": "Siguiente" });
      entries.push({ "type": "blue", "decision": "no", "label": "Ninguna opción es la correcta" });
      console.log(entries)
      return {
        id: val.qid,
        sections: [ {
          "type": "item",
          "q": val.qid
        }],
        controls: [{
            "type": "buttons",
            "entries": entries
        }]
      };
    })
  );
  console.log(tiles)
  return {
    "tiles" : tiles
  }
}

async function createOptions(qid, country){
  console.log(country)
  let cities = await getCities(country)
  let options = cities.map(function (val, index) {
    let cityId = val.id.replaceAll("Q","")
    return {
      "type": "green",
      "decision": "yes",
      "label": val.nombre,
      "api_action": {
          "action": "wbcreateclaim",
          "entity": qid,
          "property": "P19",
          "snaktype": "value",
          "value": `{\"entity-type\":\"item\",\"numeric-id\":${cityId}}`
      }
    }
  });
  return options
}

async function getCities(country) {
  maxOptions = 10
  sqlQuery = `SELECT * 
    FROM ciudad
    WHERE pais = ${country}
    ORDER BY poblacion DESC
    LIMIT ${maxOptions};`

  let cities = await queryDatabase(sqlQuery)
  return cities
}

function queryDatabase(sqlQuery) {
  return new Promise((resolve, reject) => {
    connection.query(sqlQuery, (err, rows, fields) => {
      if (err) {
        console.log("FALLÓ LA QUERY")
        reject(err);
      } else {
        resolve(rows);
      }
    });
  });
}

function logNo(id, user) {
  console.log("log no")
  sqlQuery = `UPDATE tiles
  SET status = ${STATUS_INVALID}, usuario = '${user}'
  WHERE qid = '${id}';`;
  console.log(sqlQuery)
  queryDatabase(sqlQuery);
}

function logYes(id, user) {
  console.log("log yes")
  sqlQuery = `UPDATE tiles
  SET status = ${STATUS_SOLVED}, usuario = '${user}'
  WHERE qid = '${id}';`;
  console.log(sqlQuery)
  queryDatabase(sqlQuery);
}

async function handleRequest(req, res) {
  try {
    const action = req.query.action;
    const callback = req.query.callback;
    switch (action) {
      case "desc":
        let descJson = {
          "label" : {
            "en" : "Latin America in Wikidata Contest 2023 - Artists",
            "es" : "Concurso Latinoamerica en Wikidata 2023 - Artistas",
            "pt" : "Concurso América Latina na Wikidata - Artistas"
          },
          "description" : {
            "en" : "In the 2023 edition of the Latin America in Wikidata contest, we invite you to contribute to Wikidata by improving content related to cultural heritage in Latin America.</br>There are two modes of the game. In this one, you will have to complete the birthplace of Latin American artists. You can click the small buttons below the item's name to read the description of the element or the beginning of the Wikipedia article: maybe the answer is there!</br>If you know the correct answer but it does not appear among the options, you can click the artist's name to go to the item on Wikidata and edit this property manually. Manual edits do not count for the contest but are a great contribution.</br>The contest runs from November 14, 2023, at 00:01 UTC to December 14, 2023, at 23:59 UTC, or until all available items are exhausted. This initiative is a collaboration between Wikimedia Argentina, Wikimedistas de Bolivia, Wikimedia Chile, Wikimedia Colombia, Wikimedia Mexico, Wikimedistas de Uruguay, and Wikimedia Venezuela.",
            "es" : "En la edición 2023 del concurso Latinoamérica en Wikidata te invitamos a contribuir en Wikidata mejorando contenido vinculado al patrimonio cultural en América Latina.</br>Hay dos modalidades del juego. En esta, tendrás que completar el lugar de nacimiento de artistas latinoamericanas. Puedes tocar los botones pequeños debajo del nombre del item para leer la descripción del elemento o el comienzo del artículo de Wikipedia: ¡quizás allí se encuentre la respuesta!</br>Si sabes la respuesta correcta pero no aparece entre las opciones, puedes tocar el nombre del artista para ir al elemento en Wikidata y editar esta propiedad manualmente. Las ediciones manuales no cuentan para el concurso pero son una gran contribución.</br>El concurso es desde el 14 de noviembre 2023 a las 00:01 UTC al 14 de diciembre 2023 a las 23:59 UTC, o hasta agotar los ítems disponibles. Surge de la colaboración entre Wikimedia Argentina, Wikimedistas de Bolivia, Wikimedia Chile, Wikimedia Colombia, Wikimedia Mexico, Wikimedistas de Uruguay y Wikimedia Venezuela.",
            "pt" : "Na edição de 2023 do concurso América Latina na Wikidata, convidamos você a contribuir no Wikidata melhorando o conteúdo relacionado ao patrimônio cultural na América Latina.</br>Há duas modalidades de jogo. Nesta, você terá que completar o local de nascimento de artistas latino-americanos. Você pode clicar nos botões pequenos abaixo do nome do item para ler a descrição do elemento ou o início do artigo da Wikipedia: talvez a resposta esteja lá!</br>Se você souber a resposta correta, mas ela não aparecer entre as opções, você pode clicar no nome do artista para ir ao elemento no Wikidata e editar essa propriedade manualmente. As edições manuais não contam para o concurso, mas são uma grande contribuição.</br>O concurso ocorre de 14 de novembro de 2023 às 00:01 UTC a 14 de dezembro de 2023 às 23:59 UTC, ou até que todos os itens disponíveis se esgotem. Esta iniciativa é resultado da colaboração entre a Wikimedia Argentina, Wikimedistas de Bolivia, Wikimedia Chile, Wikimedia Colombia, Wikimedia Mexico, Wikimedistas de Uruguay e Wikimedia Venezuela.",
          },
          "icon" : 'https://upload.wikimedia.org/wikipedia/commons/f/fc/GLAMWikiConfPaintIcon.png'
        }
        res.send(`${callback}(${JSON.stringify(descJson)})`);
        break;
      
      case "tiles":
        console.log("requested tiles")
        const num = req.query?.num ? req.query.num : 5;
        
        sqlQuery = `SELECT * 
        FROM tiles
        WHERE status = ${STATUS_NOT_PLAYED}
        ORDER BY RAND()
        LIMIT ${num};`

        let authors = await queryDatabase(sqlQuery)
        tiles = await createTiles(authors);
        console.log("tiles")
        console.log(tiles)
        res.send(`${callback}(${JSON.stringify(tiles)})`);
        break;

      case "log_action":
        let decision = req.query.decision
        let user = req.query.user
        let tile = req.query.tile
        console.log("made a decision")
        console.log(decision)
        
        if (decision=="yes") {
          logYes(tile,user)
        } else if (decision=="no"){
          logNo(tile,user)
        }
        break;
    
      default:
        res.json({
          error: 'Parámetro "action" inválido'
        });
    }
  } catch (error) {
    console.error('Uncaught Exception:', error);
  }
}

process.on('uncaughtException', (error) => {
  console.error('Uncaught Exception:', error);
  // Realizar acciones de limpieza si es necesario
  process.exit(1); // Forzar la salida de la aplicación con un código de error
});